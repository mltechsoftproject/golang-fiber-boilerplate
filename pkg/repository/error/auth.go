package repository_error

const (
	// Token hết hạn.
	ExpiredToken string = "Token đã hết hạn"
	// Session hết hạn.
	ExpiredSession string = "Session đã hết hạn"
)
