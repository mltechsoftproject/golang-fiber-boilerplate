package repository_credentials

const (
	//User
	UserGetListCredential                 string = "user:get-list"
	UserChangePasswordCredential          string = "user:change-password"
	UserCreateBrandCredential             string = "user:create-brand"
	UserUpdateBrandCredential             string = "user:update-brand"
	UserCreateGroupNotificationCredential string = "user:create-group-notification"
	UserUpdateGroupNotificationCredential string = "user:update-group-notification"

	// Wallet
	WalletCreateCredential      string = "wallet:create"
	WalletUpdateCredential      string = "wallet:update"
	WalletUpdatStatusCredential string = "wallet:update_Status"

	// WalletType
	WalletTypeCreateCredential       string = "wallet_type:create"
	WalletTypeUpdateCredential       string = "wallet_type:update"
	WalletTypeUpdateStatusCredential string = "wallet_type:update_status"
	WalletTypeDeleteCredential       string = "wallet_type:delete"
	WalletTypeRankCreateCredential   string = "wallet_type_rank:create"

	//Transaction
	TransactionCreateCredential string = "transaction:create"

	//Event
	EventCreateCredential string = "event:create"
	EventUpdateCredential string = "event:update"

	//Code
	CodeCreateCredential string = "code:create"
	CodeGetCredential    string = "code:get"

	//Feedback
	FeedbackCreateCredential string = "feedback:create"
	FeedbackGetCredential    string = "feedback:get"

	//Export
	ExportExcel string = "export:get"

	//Admin get
	AdminGet string = "admin:get"
)
