package repository_models

type EventType string

const (
	Donation          EventType = "donation"
	Loyalty           EventType = "loyalty"
	Eventy_Attendance EventType = "attendance"
)
