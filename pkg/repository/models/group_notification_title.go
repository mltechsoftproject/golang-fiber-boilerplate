package repository_models

type GroupNotificationTitle string

const (
	EarnTitle     GroupNotificationTitle = "Kiếm điểm"
	TransferTitle GroupNotificationTitle = "Chuyển điểm"
	ReceiveTitle  GroupNotificationTitle = "Nhận điểm"
	DonateTitle   GroupNotificationTitle = "Quyên góp"
)
