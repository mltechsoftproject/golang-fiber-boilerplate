package repository_models

type Status string

const (
	Active   Status = "active"
	Inactive Status = "inactive"
	Deleted  Status = "deleted"
)
