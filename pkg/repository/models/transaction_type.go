package repository_models

type TransactionType string

const (
	Earn       TransactionType = "earn"
	Transfer   TransactionType = "transfer"
	Receive    TransactionType = "receive"
	Donate     TransactionType = "donate"
	Attendance TransactionType = "attendance"
)

type AccountType string

const (
	AppAuthV1   AccountType = "app_auth_v1"
	AppAuthV2   AccountType = "app_auth_v2"
	AppNoAuth   AccountType = "app_no_auth"
	LandingPage AccountType = "landing_page"
)
