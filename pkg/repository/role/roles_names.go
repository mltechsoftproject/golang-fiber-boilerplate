package repository_role

const (
	// AdminRoleName const for admin role.
	AdminRoleName string = "admin"

	// StaffRoleName const for moderator role.
	StaffRoleName string = "staff"

	// UserRoleName const for user role.
	UserRoleName string = "user"
)
