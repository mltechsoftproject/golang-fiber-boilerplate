package routes

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/monitor"
)

// SwaggerRoute func for describe group of API Docs routes.
func MonitorRoute(a *fiber.App) {
	// Create routes group.
	route := a.Group("/monitor")

	// Routes for GET method:
	route.Get("", monitor.New()) // get one user by ID
}
