package routes

import (
	"backend-qr-code/app/controllers"
	"backend-qr-code/pkg/middleware"

	"github.com/gofiber/fiber/v2"
)

// PrivateRoutes func for describe group of private routes.
func PrivateRoutes(a *fiber.App) {
	// Create routes group.
	route := a.Group("/api/v1")

	// Routes for GET method:
	route.Get("/user/profile", middleware.JWTProtected(), controllers.GetUserProfile) // get user profile
	route.Get("/user/list", middleware.JWTProtected(), controllers.GetListUser)       // get list user

	// Routes for POST method:// get event slot by id
	route.Post("/token/renew", middleware.JWTProtected(), controllers.RenewTokens) // renew Access & Refresh tokens

	// Routes for PUT method:
	route.Put("/user", middleware.JWTProtected(), controllers.UpdateProfile)                  // update user profile
	route.Put("/user-change-password", middleware.JWTProtected(), controllers.ChangePassword) // update user password

	// Routes for EXPORT excel file:
	route.Get("/export-excel/event-code", middleware.JWTProtected(), controllers.ExportCodeExcel) // export event code by event_id
}
