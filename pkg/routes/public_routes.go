package routes

import (
	"backend-qr-code/app/controllers"

	"github.com/gofiber/fiber/v2"
)

// PublicRoutes func for describe group of public routes.
func PublicRoutes(a *fiber.App) {
	// Create routes group.
	route := a.Group("/api/v1")

	// Routes for GET method:
	route.Get("/export/generate-code", controllers.ExportCodeExcel) // generate code -> export excel

	// Routes for POST method:
	route.Post("/auth/check-account", controllers.CheckUserExists) // register a new user
	route.Post("/auth/sign/up", controllers.UserSignUp)            // register a new user
	route.Post("/auth/sign/in", controllers.UserSignIn)            // auth, return Access & Refresh tokens

}
