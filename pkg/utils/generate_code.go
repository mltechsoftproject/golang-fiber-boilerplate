package utils

import (
	"fmt"
	"math/rand"
	"strings"
	"time"
)

var (
	lowerCharSet = "abcdefghijklmnopqrstuvwxyz"
	upperCharSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	numberSet    = "0123456789"
	allCharSet   = lowerCharSet + upperCharSet + numberSet
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func shuffleString(s string) string {
	r := []rune(s)
	rand.Seed(time.Now().UnixNano())

	for i := len(r) - 1; i > 0; i-- {
		j := rand.Intn(i + 1)
		r[i], r[j] = r[j], r[i]
	}

	return string(r)
}

func RandStringBytes(n int) string {
	letters := shuffleString(letterBytes)
	b := make([]byte, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func GenerateUniqueCode(passwordLength, minNum, minLowerCase int) string {
	var password strings.Builder

	// set numberic character
	if minNum != 0 {
		for i := 0; i < minNum; i++ {
			random := rand.Intn(len(numberSet))
			password.WriteString(string(numberSet[random]))
		}
	}

	// set uppercase character
	if minLowerCase != 0 {
		for i := 0; i < minLowerCase; i++ {
			random := rand.Intn(len(upperCharSet))
			password.WriteString(string(upperCharSet[random]))
		}
	}

	remainingLength := passwordLength - minNum - minLowerCase
	for i := 0; i < remainingLength; i++ {
		random := rand.Intn(len(allCharSet))
		password.WriteString(string(allCharSet[random]))
	}
	// Shuffle
	shuffleString := []rune(password.String())
	rand.Shuffle(len(shuffleString), func(i, j int) {
		shuffleString[i], shuffleString[j] = shuffleString[j], shuffleString[i]
	})
	return string(shuffleString)
}

func checkContainInSlice(input []string, searchString string) bool {
	// initialize a found flag
	found := false
	// iterate over the slice
	for i, v := range input {
		// check if the strings match
		if v == searchString {
			found = true
			fmt.Println("The slice contains", searchString, "at index", i)
			break
		}
	}
	return found
}

func removeDuplicates(s []string) []string {
	unique := make(map[string]bool)
	result := []string{}

	for _, v := range s {
		if !unique[v] {
			unique[v] = true
			result = append(result, v)
		}
	}

	return result
}
