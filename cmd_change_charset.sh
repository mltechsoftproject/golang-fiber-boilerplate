docker exec -it qrcode_db_mysql  bash
mysqldump --user=root --password=password --default-character-set=latin1 --skip-set-charset qrcode > dump.sql
sed -r 's/latin1/utf8mb4/g' dump.sql > dump_utf.sql
mysql --user=root --password=password --execute="DROP DATABASE qrcode; CREATE DATABASE qrcode CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
mysql --user=root --password=password --default-character-set=utf8mb4 qrcode < dump_utf.sql
