package controllers

import (
	"backend-qr-code/app/models"
	queries "backend-qr-code/app/queries"
	repository_credentials "backend-qr-code/pkg/repository/credentials"
	"backend-qr-code/pkg/utils"
	"backend-qr-code/services/auth"
	"backend-qr-code/services/common"
	"time"

	"github.com/gofiber/fiber/v2"
)

// GetUserProfile method to get user profile.
// @Router /v1/user/profile
func GetUserProfile(c *fiber.Ctx) error {
	// Get claims from JWT.
	claims, err := auth.ExtractTokenMetadata(c)
	if err != nil {
		// Return status 500 and JWT parse error.
		return common.NewErrResponse(c, 500, err.Error())
	}

	// Define user ID.
	userID := claims.UserID

	// Get user by userID.
	foundedUser, err := queries.GetUserByID(userID)
	if err != nil {
		// Return, if user not found.
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
			"error": true,
			"msg":   "user is not found",
			"data":  nil,
		})
	}

	// Delete password hash field from JSON view.
	foundedUser.PasswordHash = ""

	// Return status 200 OK.
	return c.JSON(fiber.Map{
		"error": false,
		"msg":   nil,
		"data":  foundedUser,
	})
}

// GetUserProfile method to get user profile.
// @Router /v1/user/find
func GetUserByPhone(c *fiber.Ctx) error {
	// Get claims from JWT.
	_, err := auth.ExtractTokenMetadata(c)
	if err != nil {
		// Return status 500 and JWT parse error.
		return common.NewErrResponse(c, 500, err.Error())
	}

	query := &models.UserGetQuery{}
	// Checking received data from JSON body.
	if err := c.QueryParser(query); err != nil {
		// Return status 400 and error message.
		return common.NewErrResponse(c, 400, err.Error())
	}

	// Create a new validator.
	validate := utils.NewValidator()
	// Validate sign up fields.
	if err := validate.Struct(query); err != nil {
		// Return, if some fields are not valid.
		return common.NewErrResponse(c, 400, utils.ValidatorErrors(err))
	}

	// Get user by phone.
	user, err := queries.GetUserByPhone(query.Phone)
	if err != nil {
		// Return, if user not found.
		return common.NewErrResponse(c, 400, "Không tìm thấy người dùng")
	}
	// Delete password hash field from JSON view.
	user.PasswordHash = ""
	// Return status 200 OK.
	return common.NewSimpleResponse(c, 200, nil, user)
}

// Admin - get all users
func GetListUser(c *fiber.Ctx) error {
	// Get claims from JWT.
	claims, err := auth.ExtractTokenMetadata(c)
	if err != nil {
		// Return status 500 and JWT parse error.
		return common.NewErrResponse(c, 500, err.Error())
	}

	// Kiểm tra credential = true/false từ JWT -> để xem user giữ token có quyền gì
	// Get user by ID.
	foundedUser, err := queries.GetUserByID(claims.UserID)

	// Get role credentials from founded user.
	credentials, err := auth.GetCredentialsByRole(foundedUser.UserRole)
	credential := credentials[repository_credentials.UserGetListCredential]
	if !credential {
		// Return status 403 and permission denied error message.
		return common.NewErrResponse(c, 403, "permission denied, check credentials of your token")
	}
	filter := &models.UserFilter{}
	if err := c.QueryParser(filter); err != nil {
		// Return status 400 and error message.
		return common.NewErrResponse(c, 400, err.Error())
	}

	validate := utils.NewValidator()
	// Validate sign up fields.
	if err := validate.Struct(filter); err != nil {
		// Return, if some fields are not valid.
		return common.NewErrResponse(c, 400, utils.ValidatorErrors(err))
	}
	var paging common.Pagination
	if err := c.QueryParser(&paging); err != nil {
		// Return status 400 and error message.
		return common.NewErrResponse(c, 400, err.Error())
	}
	paging.Preload()
	users, err := queries.GetUsers(filter, &paging)
	if err != nil {
		// Return, if user not found.
		return common.NewErrResponse(c, 500, err.Error())
	}
	// Return status 200 OK.
	return common.NewSuccessResponse(c, 200, nil, users, paging, filter)
}

// update profile
func UpdateProfile(c *fiber.Ctx) error {
	// Get claims from JWT.
	claims, err := auth.ExtractTokenMetadata(c)
	if err != nil {
		// Return status 500 and JWT parse error.
		return common.NewErrResponse(c, fiber.StatusInternalServerError, err.Error())
	}

	// Define user ID.
	userID := claims.UserID

	// Get user by userID.
	foundedUser, err := queries.GetUserByID(userID)
	if err != nil {
		// Return, if user not found.
		return common.NewErrResponse(c, fiber.StatusNotFound, "Không tìm thấy người dùng")
	}

	// Delete password hash field from JSON view.
	foundedUser.PasswordHash = ""

	// Create a new user struct.
	user := &models.UserUpdateProfile{}

	// Set initialized default data for user:
	user.ID = userID
	user.CreatedAt = foundedUser.CreatedAt
	user.UpdatedAt = time.Now()
	user.Status = foundedUser.Status
	user.UserRole = foundedUser.UserRole
	user.PasswordHash = foundedUser.PasswordHash
	user.Phone = foundedUser.Phone

	if err := c.BodyParser(user); err != nil {
		// Return status 400 and error message.
		return common.NewErrResponse(c, fiber.StatusBadRequest, err.Error())
	}

	// Create a new validator.
	validate := utils.NewValidator()

	// Validate sign up fields.
	if err := validate.Struct(user); err != nil {
		// Return, if some fields are not valid.
		return common.NewErrResponse(c, fiber.StatusBadRequest, utils.ValidatorErrors(err))
	}

	if err := queries.UpdateUser(user, userID); err != nil {
		// Return status 500 and create user process error.
		return common.NewErrResponse(c, fiber.StatusInternalServerError, err.Error())
	}

	return common.NewSimpleResponse(c, 200, nil, user)
}

// change password
func ChangePassword(c *fiber.Ctx) error {
	// Get claims from JWT.
	claims, err := auth.ExtractTokenMetadata(c)
	if err != nil {
		// Return status 500 and JWT parse error.
		return common.NewErrResponse(c, fiber.StatusInternalServerError, err.Error())
	}

	// Define user ID.
	userID := claims.UserID

	// Get user by userID.
	foundedUser, err := queries.GetUserByID(userID)

	// Get role credentials from founded user.
	credentials, err := auth.GetCredentialsByRole(foundedUser.UserRole)
	credential := credentials[repository_credentials.UserChangePasswordCredential]
	if !credential {
		// Return status 403 and permission denied error message.
		return common.NewErrResponse(c, fiber.StatusBadRequest, "Người dùng này không có quyền!")
	}
	// Delete password hash field from JSON view.
	foundedUser.PasswordHash = ""

	// Create a new user struct.
	user := &models.UserUpdatePassword{}

	// Set initialized default data for user:
	user.ID = userID

	if err := c.BodyParser(user); err != nil {
		// Return status 400 and error message.
		return common.NewErrResponse(c, fiber.StatusBadRequest, err.Error())
	}

	// Create a new validator.
	validate := utils.NewValidator()

	// Validate sign up fields.
	if err := validate.Struct(user); err != nil {
		// Return, if some fields are not valid.
		return common.NewErrResponse(c, fiber.StatusBadRequest, utils.ValidatorErrors(err))
	}

	if err := queries.UpdatePassword(user, userID); err != nil {
		// Return status 500 and create user process error.
		return common.NewErrResponse(c, fiber.StatusInternalServerError, err.Error())
	}

	return common.NewSimpleResponse(c, 200, nil, "đổi mật khẩu thành công!")
}
