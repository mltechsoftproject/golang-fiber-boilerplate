package controllers

import (
	"context"
	"time"

	"backend-qr-code/app/models"
	repository_error "backend-qr-code/pkg/repository/error"
	"backend-qr-code/platform/cache"
	"backend-qr-code/services/auth"
	"backend-qr-code/services/common"

	"github.com/gofiber/fiber/v2"
)

// RenewTokens method for renew access and refresh tokens.
// @Description Renew access and refresh tokens.
// @Summary renew access and refresh tokens
// @Tags Token
// @Accept json
// @Produce json
// @Param refresh_token body string true "Refresh token"
// @Success 200 {string} status "ok"
// @Security ApiKeyAuth
// @Router /v1/token/renew [post]
func RenewTokens(c *fiber.Ctx) error {
	// Get now time.
	now := time.Now().Unix()

	// Get claims from JWT.
	claims, err := auth.ExtractTokenMetadata(c)
	if err != nil {
		// Return status 500 and JWT parse error.
		return common.NewErrResponse(c, 500, err.Error())
	}

	// Set expiration time from JWT data of current user.
	expiresAccessToken := claims.Expires

	// Checking, if now time greather than Access token expiration time.
	if now > expiresAccessToken {
		// Return status 401 and unauthorized error message.
		return common.NewErrResponse(c, 401, repository_error.ExpiredToken)
	}

	// Create a new renew refresh token struct.
	renew := &models.Renew{}

	// Checking received data from JSON body.
	if err := c.BodyParser(renew); err != nil {
		// Return, if JSON data is not correct.
		return common.NewErrResponse(c, 400, err.Error())
	}

	// Set expiration time from Refresh token of current user.
	expiresRefreshToken, err := auth.ParseRefreshToken(renew.RefreshToken)
	if err != nil {
		// Return status 400 and error message.
		return common.NewErrResponse(c, 400, err.Error())
	}

	// Checking, if now time greather than Refresh token expiration time.
	if now < expiresRefreshToken {
		// Define user ID.
		userID := claims.UserID

		// Create database connection.

		if err != nil {
			// Return, if user not found.
			return common.NewErrResponse(c, 400, repository_error.NotFoundUserById)
		}

		if err != nil {
			// Return status 400 and error message.
			return common.NewErrResponse(c, 400, err.Error())
		}

		// Generate JWT Access & Refresh tokens.
		tokens, err := auth.GenerateNewTokens(userID.String())
		if err != nil {
			// Return status 500 and token generation error.
			return common.NewErrResponse(c, 500, err.Error())
		}

		// Create a new Redis connection.
		connRedis, err := cache.RedisConnection()
		if err != nil {
			// Return status 500 and Redis connection error.
			return common.NewErrResponse(c, 500, err.Error())
		}

		// Save refresh token to Redis.
		errRedis := connRedis.Set(context.Background(), userID.String(), tokens.Refresh, 0).Err()
		if errRedis != nil {
			// Return status 500 and Redis connection error.
			return common.NewErrResponse(c, 500, errRedis.Error())
		}

		// Return status 200 OK.
		return common.NewSimpleResponse(c, 200, nil, fiber.Map{
			"access":  tokens.Access,
			"refresh": tokens.Refresh,
		})
	} else {
		// Return status 401 and unauthorized error message.
		return common.NewErrResponse(c, 401, repository_error.ExpiredSession)
	}
}
