package controllers

import (
	"backend-qr-code/app/models"
	queries "backend-qr-code/app/queries"
	repository_credentials "backend-qr-code/pkg/repository/credentials"
	"backend-qr-code/pkg/utils"
	"backend-qr-code/platform/cache"
	"backend-qr-code/services/auth"
	"backend-qr-code/services/common"
	services_export "backend-qr-code/services/export"
	"context"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/gofiber/fiber/v2"
	"github.com/shirou/gopsutil/process"
)

func getCPUMemUsage() (float64, int64, error) {
	p, err := process.NewProcess(int32(os.Getpid()))
	if err != nil {
		return 0, 0, err
	}

	cpuPercent, err := p.CPUPercent()
	if err != nil {
		return 0, 0, err
	}

	memInfo, err := p.MemoryInfo()
	if err != nil {
		return 0, 0, err
	}

	return cpuPercent, int64(memInfo.RSS), nil
}

// ExportCodeExcel method to export codes by event_id - send buffer to client
func ExportCodeExcel(c *fiber.Ctx) error {
	// Get claims from JWT.
	claims, err := auth.ExtractTokenMetadata(c)
	if err != nil {
		// Return status 500 and JWT parse error.
		return common.NewErrResponse(c, fiber.StatusInternalServerError, err.Error())
	}
	// Kiểm tra `ExportExcel:get` true/false từ JWT -> để xem user giữ token có quyền gì
	// Get user by ID.
	foundedUser, err := queries.GetUserByID(claims.UserID)
	// Get role credentials from founded user.
	credentials, err := auth.GetCredentialsByRole(foundedUser.UserRole)

	credential := credentials[repository_credentials.ExportExcel]
	// Chỉ có user có `ExportExcel:get` credential mới có thể lấy file export.
	if !credential {
		// Return status 403 and permission denied error message.
		return common.NewErrResponse(c, fiber.StatusForbidden, "Người dùng không có quyền")
	}
	// Create a new check users exists struct.
	filter := &models.ExportCodeByEventID{}
	// Checking received data from JSON body.
	if err := c.QueryParser(filter); err != nil {
		// Return status 400 and error message.
		return common.NewErrResponse(c, 400, err.Error())
	}

	validate := utils.NewValidator()
	if err := validate.Struct(filter); err != nil {
		// Return, if some fields are not valid.
		return common.NewErrResponse(c, 400, utils.ValidatorErrors(err))
	}

	countCodes := queries.CountNumberOfCodeByEventID(*filter.EventID)
	if countCodes == 0 {
		return common.NewErrResponse(c, 400, "Chương trình chưa tạo mã code!")
	}

	// Create a new Redis connection.
	connRedis, err := cache.RedisConnection()
	if err != nil {
		// Return status 500 and Redis connection error.
		return common.NewErrResponse(c, 400, err.Error())
	}

	lenInCacheString, err := connRedis.Get(context.Background(), filter.EventID.String()).Result()
	if err == redis.Nil {
		lenInCacheString = ""
	}

	lenInCache, err := strconv.Atoi(lenInCacheString)
	if err != nil {
		lenInCache = 0
	}

	// Đường dẫn của file excel đã build (nếu có)
	filePath := ""

	downloadName := "Code_event_" + filter.EventName + "_" + time.Now().Local().Format("data-20060102150405.xlsx")
	// Thiết lập header để trình duyệt hiểu rằng đây là file Excel
	// c.Response().Header.Set("Content-Type", "text/csv")
	c.Set(fiber.HeaderContentType, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	c.Set(fiber.HeaderContentDisposition, "attachment; filename="+downloadName)

	if lenInCache == countCodes {
		file, _ := connRedis.Get(context.Background(), filter.EventID.String()+":filename").Result()
		filePath = file
	}

	if filePath == "" {
		codes := queries.GetOnlyCodeByEventID(filter)

		// Save len of codes to Redis.
		if errSaveToRedis := connRedis.Set(context.Background(), filter.EventID.String(), len(codes), 0).Err(); errSaveToRedis != nil {
			// Return status 500 and Redis connection error.
			return common.NewErrResponse(c, 500, errSaveToRedis.Error())
		}

		startCPU, startMem, err := getCPUMemUsage()
		file, err := services_export.CodesStreamExportExcel(codes, filter.EventName)

		if err != nil {
			log.Println(err)
		}

		endCPU, endMem, err := getCPUMemUsage()
		cpuUsage := endCPU - startCPU
		memUsage := endMem - startMem

		log.Printf("CPU usage: %.2f\n", cpuUsage)
		log.Printf("Memory usage: %d mb\n", memUsage/1024/1024)

		if err != nil {
			return common.NewErrResponse(c, 500, err.Error())
		}

		// Save filename to Redis.
		if errSaveToRedis := connRedis.Set(context.Background(), filter.EventID.String()+":filename", file, 0).Err(); errSaveToRedis != nil {
			// Return status 500 and Redis connection error.
			return common.NewErrResponse(c, 500, errSaveToRedis.Error())
		}
		// Gửi file Excel về client
		return c.SendFile(file)
	} else {
		// overwrite len of codes to Redis.
		if errSaveToRedis := connRedis.Set(context.Background(), filter.EventID.String(), countCodes, 0).Err(); errSaveToRedis != nil {
			// Return status 500 and Redis connection error.
			return common.NewErrResponse(c, 500, errSaveToRedis.Error())
		}
		// Gửi file Excel về client
		return c.SendFile(filePath)
	}
}
