package controllers

import (
	"time"

	"backend-qr-code/app/models"
	queries "backend-qr-code/app/queries"
	repository_models "backend-qr-code/pkg/repository/models"
	repository_role "backend-qr-code/pkg/repository/role"
	"backend-qr-code/pkg/utils"

	"backend-qr-code/services/auth"
	"backend-qr-code/services/common"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

// CheckUserExists method to check user exists.
// @Description Check user exists.
// @Summary Check user exists
// @Tags User
// @Accept json
// @Produce json
// @Param phone body string true "Phone"
// @Success 200 boolean true or false
// @Router /v1/auth/check-account [post]

func CheckUserExists(c *fiber.Ctx) error {
	// Create a new check users exists struct.
	checkExists := &models.CheckExists{}
	// Checking received data from JSON body.
	if err := c.BodyParser(checkExists); err != nil {
		// Return status 400 and error message.
		return common.NewErrResponse(c, fiber.StatusBadRequest, err.Error())
	}

	validate := utils.NewValidator()

	if err := validate.Struct(checkExists); err != nil {
		// Return, if some fields are not valid.
		return common.NewErrResponse(c, fiber.StatusBadRequest, utils.ValidatorErrors(err))
	}

	// Get user by phone.
	user, err := queries.GetUserByPhone(checkExists.Phone)
	if err != nil {
		// Return, if user not found.
		return common.NewSimpleResponse(c, 200, "Số điện thoại chưa đăng ký", nil)
	}

	// Return, if user found.
	return common.NewSimpleResponse(c, 200, "Tài khoản đã tồn tại", map[string]interface{}{
		"full_name": user.FullName,
	})

}

// UserSignUp method to create a new user.
// @Description Create a new user.
// @Summary create a new user
// @Tags User
// @Accept json
// @Produce json
// @Param phone body string true "Phone"
// @Param password body string true "Password"
// @Param user_role body string true "User role"
// @Success 200 {object} models.User
// @Router /v1/auth/sign/up [post]

func UserSignUp(c *fiber.Ctx) error {
	// Create a new user auth struct.
	signUp := &models.SignUp{}

	// Checking received data from JSON body.
	if err := c.BodyParser(signUp); err != nil {
		// Return status 400 and error message.
		return common.NewErrResponse(c, 400, err.Error())
	}

	// Create a new validator for a User model.
	validate := utils.NewValidator()

	// Validate sign up fields.
	if err := validate.Struct(signUp); err != nil {
		// Return, if some fields are not valid.
		return common.NewErrResponse(c, fiber.StatusBadRequest, utils.ValidatorErrors(err))
	}

	// Create a new user struct.
	user := &models.User{}

	// Set initialized default data for user:
	user.ID = uuid.New()
	user.CreatedAt = time.Now()
	user.UpdatedAt = time.Now()
	user.Phone = signUp.Phone
	user.Email = signUp.Email
	user.FullName = signUp.FullName
	user.Gender = signUp.Gender
	user.PasswordHash = auth.GeneratePassword(signUp.Password)
	user.Status = repository_models.Active
	user.UserRole = repository_role.UserRoleName

	// Validate user fields.
	if err := validate.Struct(user); err != nil {
		// Return, if some fields are not valid.
		return common.NewErrResponse(c, 400, utils.ValidatorErrors(err))
	}

	// Create a new user with validated data.
	if err := queries.CreateUser(user); err != nil {
		// Return status 500 and create user process error.
		return common.NewErrResponse(c, 500, err.Error())
	}

	// Delete password hash field from JSON view.
	user.PasswordHash = ""

	// Return status 200 OK.
	return common.NewSimpleResponse(c, 200, nil, user)

}

// UserSignIn method to auth user and return access and refresh tokens.
// @Description Auth user and return access and refresh token.
// @Summary auth user and return access and refresh token
// @Tags User
// @Accept json
// @Produce json
// @Param phone body string true "User Phone"
// @Param password body string true "User Password"
// @Success 200 {string} status "ok"
// @Router /v1/auth/sign/in [post]
func UserSignIn(c *fiber.Ctx) error {
	// Create a new user auth struct.
	signIn := &models.SignIn{}

	// Checking received data from JSON body.
	if err := c.BodyParser(signIn); err != nil {
		// Return status 400 and error message.
		return common.NewErrResponse(c, 400, err.Error())
	}

	// Get user by phone.
	foundedUser, err := queries.GetUserByPhone(signIn.Phone)
	if err != nil {
		// Return, if user not found.
		return common.NewErrResponse(c, 400, "Không tìm thấy số điện thoại đăng ký")
	}

	// Compare given user password with stored in found user.
	compareUserPassword := auth.ComparePasswords(foundedUser.PasswordHash, signIn.Password)
	if !compareUserPassword {
		// Return, if password is not compare to stored in database.
		return common.NewErrResponse(c, 400, "Sai mật khẩu")
	}

	if err != nil {
		// Return status 400 and error message.
		return common.NewErrResponse(c, 400, err.Error())
	}

	// Generate a new pair of access and refresh tokens.
	tokens, err := auth.GenerateNewTokens(foundedUser.ID.String())
	if err != nil {
		// Return status 500 and token generation error.
		return common.NewErrResponse(c, 500, err.Error())
	}

	//

	// Return status 200 OK.
	return common.NewSimpleResponse(c, 200, nil, fiber.Map{
		"access":  tokens.Access,
		"refresh": tokens.Refresh,
	})
}
