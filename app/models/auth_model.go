package models

// SignUp struct to describe register a new user.
type SignUp struct {
	Phone    string `json:"phone" validate:"required"`
	Password string `json:"password" validate:"required,lte=255"`
	UserRole string `json:"user_role" validate:"required,lte=25"`
	Gender   string `json:"gender" validate:"required"`
	FullName string `json:"full_name" validate:"required"`
	Email    string `json:"email"`
}

// SignIn struct to describe login user.
type CheckExists struct {
	Phone string `json:"phone" validate:"required"`
}

type SignIn struct {
	Phone       string `json:"phone" validate:"required"`
	Password    string `json:"password" validate:"required,lte=255"`
	DeviceID    string `json:"device_id"`
	DeviceToken string `json:"device_token"`
	Platform    string `json:"platform"`
}
