package models

import (
	repository_models "backend-qr-code/pkg/repository/models"
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type BaseModel struct {
	ID        uuid.UUID                `json:"id" gorm:"type:char(36);primary_key"`
	Status    repository_models.Status `json:"status" gorm:"status;default:active"`
	CreatedAt time.Time                `json:"created_at" gorm:"autoCreateTime"`
	UpdatedAt time.Time                `json:"updated_at" gorm:"autoUpdateTime"`
	DeletedAt gorm.DeletedAt           `json:"deleted_at" gorm:"autoDeleteTime"`
}
