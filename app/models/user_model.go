package models

import "github.com/google/uuid"

// User struct to describe User object.
type User struct {
	BaseModel
	Phone        string `json:"phone" validate:"required,len=10,numeric,startswith=0" gorm:"unique"`
	PasswordHash string `json:"-" validate:"required,lte=255"`
	UserRole     string `json:"user_role" validate:"required,lte=25"`
	Gender       string `json:"gender"`
	FullName     string `json:"full_name"`
	Email        string `json:"email"`
}

type UserGetQuery struct {
	Phone string `query:"phone"  validate:"required,len=10,numeric,startswith=0"`
}

func (User) TableName() string {
	return "users"
}

type UserUpdateProfile struct {
	BaseModel
	Phone        string `json:"phone" validate:"required,len=10,numeric,startswith=0"`
	PasswordHash string `json:"password_hash,omitempty"`
	UserRole     string `json:"user_role"`
	Gender       string `json:"gender"`
	FullName     string `json:"full_name"`
	Email        string `json:"email"`
}

func (UserUpdateProfile) TableName() string {
	return User{}.TableName()
}

type UserUpdatePassword struct {
	ID       uuid.UUID `json:"id" gorm:"type:char(36);primary_key"`
	Password string    `json:"password" validate:"required,lte=255"`
}

type UserFilter struct {
	Phone        string `query:"phone"`
	UserRole     string `query:"user_role"`
	Gender       string `query:"gender"`
	FullName     string `query:"full_name"`
	Email        string `query:"email"`
	Point        int    `query:"point" `
	DonatePoint  int    `query:"donate_point"`
	EarnPoint    int    `query:"earn_point" `
	WalletTypeID string `query:"wallet_type_id"`
	BrandID      string `query:"brand_id"`
}
