package models

import "github.com/google/uuid"

type Code struct {
	BaseModel
	Description string    `json:"description"`
	EventID     uuid.UUID `json:"event_id"`
	Event       Event     `json:"event"`
	Code        string    `json:"code" gorm:"unique"`
}

type GenerateCodeWithEvent struct {
	Prefix  string    `json:"prefix" validate:"required"`
	Amount  int       `json:"amount" validate:"required"`
	EventID uuid.UUID `json:"event_id" validate:"required"`
}

func (Code) TableName() string {
	return "codes"
}
