package models

import (
	repository_models "backend-qr-code/pkg/repository/models"
	"time"

	"github.com/google/uuid"
)

type Event struct {
	BaseModel
	Name                   string                      `json:"name"`
	Description            string                      `json:"description"`
	PublishDate            time.Time                   `json:"publish_date"`
	RegistrationStartDate  time.Time                   `json:"registration_start_date"`
	RegistrationExpireDate time.Time                   `json:"registration_expire_date"`
	ExpireDate             time.Time                   `json:"expire_date"`
	EventType              repository_models.EventType `json:"event_type"`
	Limit                  int                         `json:"limit"`
	Registered             bool                        `json:"registered"`
	Thumbnail              string                      `json:"thumbnail"`
	Address                string                      `json:"address"`
	ContentID              string                      `json:"content_id"`
	Content                string                      `json:"content"`
	TotalDonations         int                         `json:"total_donations"`
	NumberOfDonations      int                         `json:"number_of_donations"`
	Point                  int                         `json:"point"`
	NumberOfAccum          int                         `json:"number_of_accum"`
	NumberOfCodes          int                         `json:"number_of_codes"`
	NumberOfSlots          int                         `json:"number_of_slots"`
	TaskID                 string                      `json:"task_id"`
	Codes                  []Code                      `json:"-"`
}

type EventCreate struct {
	BaseModel
	Name                   string                      `json:"name" validate:"required"`
	Description            string                      `json:"description" validate:"required"`
	PublishDate            time.Time                   `json:"publish_date" validate:"required"`
	ExpireDate             time.Time                   `json:"expire_date" validate:"required"`
	RegistrationStartDate  *time.Time                  `json:"registration_start_date"`
	RegistrationExpireDate *time.Time                  `json:"registration_expire_date"`
	WalletTypeID           uuid.UUID                   `json:"wallet_type_id" validate:"required"`
	EventType              repository_models.EventType `json:"event_type" validate:"required"`
	Limit                  int                         `json:"limit"`
	Registered             bool                        `json:"registered"`
	Thumbnail              string                      `json:"thumbnail" validate:"required"`
	Content                string                      `json:"content" validate:"required"`
	TotalDonations         int                         `json:"total_donations"`
	Point                  int                         `json:"point"`
	ContentID              string                      `json:"content_id"`
	NumberOfDonations      int                         `json:"number_of_donations"`
}

type EventFilters struct {
	ID           string                      `json:"id" query:"id"`
	Name         string                      `json:"name" query:"name"`
	Status       string                      `json:"status" query:"status"`
	EventType    repository_models.EventType `json:"event_type" query:"event_type"`
	Expired      string                      `json:"expired,omitempty" query:"expired"`
	WalletTypeID string                      `json:"wallet_type_id" query:"wallet_type_id"`
}

func (Event) TableName() string {
	return "events"
}

func (EventCreate) TableName() string {
	return Event{}.TableName()
}
