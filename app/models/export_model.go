package models

import "github.com/google/uuid"

// SignUp struct to describe generate excel for genarated code.
type ExportCodeByEventID struct {
	EventID   *uuid.UUID `query:"event_id" validate:"required"`
	EventName string     `query:"event_name" validate:"required"`
}
