package queries

import (
	"backend-qr-code/app/models"
	repository_models "backend-qr-code/pkg/repository/models"
	"backend-qr-code/platform/database"
	"errors"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

func CountNumberOfCodeByEventID(event_id uuid.UUID) int {
	var count int64
	database.Database.Raw("SELECT COUNT(*) FROM codes c WHERE c.event_id = ?", event_id).Scan(&count)
	return int(count)
}

func UpdateCodeStatus(tx *gorm.DB, code string, status repository_models.Status) error {
	var code_model models.Code
	err := tx.Model(&code_model).Where("code = ?", code).Update("status", status).Error
	return err
}

// get code info by code
func GetCodeByCode(code string) (models.Code, error) {
	var code_info models.Code
	var model models.Code
	err := database.Database.Model(&model).Where("code = BINARY ?", code).Preload("Event").First(&code_info).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return code_info, err
	}
	return code_info, nil
}

// get code info by event_id
func GetCodeByEventID(filter *models.ExportCodeByEventID) ([]models.Code, error) {
	var code_info []models.Code
	err := database.Database.Model(&models.Code{}).Where("event_id = ?", *filter.EventID).Find(&code_info).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return code_info, err
	}
	return code_info, nil
}

// get only code in code model by event_id
func GetOnlyCodeByEventID(filter *models.ExportCodeByEventID) []models.Code {
	var code_info []models.Code
	database.Database.Raw("SELECT code FROM codes WHERE event_id = ?", *filter.EventID).Scan(&code_info)
	return code_info
}

func GetCodeByCodeTransaction(tx *gorm.DB, code string) (models.Code, error) {
	var code_info models.Code
	var model models.Code
	err := tx.Model(&model).Where("status = ? AND code = BINARY ?", repository_models.Active, code).Preload("Event").First(&code_info).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return code_info, err
	}
	return code_info, nil
}
