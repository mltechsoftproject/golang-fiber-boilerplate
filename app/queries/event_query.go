package queries

import (
	"backend-qr-code/app/models"
	repository_models "backend-qr-code/pkg/repository/models"
	"backend-qr-code/platform/database"
	"backend-qr-code/services/common"
	"errors"
	"math"
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

// get events
func GetEvents() ([]models.Event, error) {
	var events []models.Event
	var model models.Event
	err := database.Database.Model(&model).Where("status = ?", repository_models.Active).Preload("WalletType").Preload("WalletType.Brand").Find(&events).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return events, err
	}
	return events, nil
}

// get event by type
func GetEventsByType(filter *models.EventFilters, paging *common.Pagination) ([]models.Event, error) {
	var events []models.Event
	var model models.Event

	db := database.Database
	time_now := time.Now().UTC()

	if v := filter; v != nil {
		if v.ID != "" {
			db = db.Model(&model).Where("id = ?", v.ID)
		}

		if v.Status != "" {
			if v.Status != "all" {
				db = db.Model(&model).Where("status = ?", v.Status)
			}
		} else {
			db = db.Model(&model).Where("status = ?", repository_models.Active)
		}

		if v.WalletTypeID != "" {
			db = db.Model(&model).Where("wallet_type_id = ?", v.WalletTypeID)
		}
		if v.Name != "" {
			db = db.Model(&model).Where("name LIKE ?", string("%"+v.Name+"%"))
		}
		if v.Expired == "true" {
			db = db.Model(&model).Where("expire_date < ?", time_now)
		} else if v.Expired == "false" {
			db = db.Model(&model).Where("expire_date > ?", time_now).Where("publish_date < ?", time_now)
		}

	}
	//conditions
	db = db.Where("event_type = ?", filter.EventType)

	if paging != nil {
		db.Model(&model).Count(&paging.Total)
		paging.PageCount = int(math.Ceil(float64(paging.Total) / float64(paging.PageSize)))
		db = db.
			Offset((paging.Page - 1) * paging.PageSize).
			Limit(paging.PageSize)
	}
	// Send query to database.
	err := db.Model(&model).Preload("WalletType").Preload("WalletType.Brand").Find(&events).Error

	if filter.EventType == repository_models.Eventy_Attendance {
		for i := 0; i < len(events); i++ {
			var count int64
			database.Database.Raw("SELECT COUNT(*) FROM event_slots WHERE status = ? AND event_id = ?", repository_models.Active, events[i].ID).Scan(&count)
			events[i].NumberOfSlots = int(count)
		}
	} else if filter.EventType == repository_models.Loyalty {
		for i := 0; i < len(events); i++ {
			if int(events[i].NumberOfCodes) == 0 {
				var count int64
				database.Database.Raw("SELECT COUNT(*) FROM codes WHERE event_id = ?", events[i].ID).Scan(&count)
				events[i].NumberOfCodes = int(count)
			}

		}
	}

	if errors.Is(err, gorm.ErrRecordNotFound) {
		return events, err
	}
	return events, nil
}

func CreateEvent(event *models.EventCreate) error {
	err := database.Database.Create(&event).Error
	return err
}

func GetEventById(id uuid.UUID) (models.Event, error) {
	var event models.Event
	var model models.Event

	err := database.Database.Model(&model).Where("id = ?", id).Preload("WalletType").Preload("WalletType.Brand").First(&event).Error

	if event.EventType == repository_models.Eventy_Attendance {
		var count int64
		database.Database.Raw("SELECT COUNT(*) FROM event_slots e WHERE e.status = ? AND e.event_id = ?", repository_models.Active, event.ID).Scan(&count)
		event.NumberOfSlots = int(count)

	} else if event.EventType == repository_models.Loyalty && int(event.NumberOfCodes) == 0 {
		var count int64
		database.Database.Raw("SELECT COUNT(*) FROM codes c WHERE c.event_id = ?", event.ID).Scan(&count)
		event.NumberOfCodes = int(count)
	}

	if errors.Is(err, gorm.ErrRecordNotFound) {
		return event, err
	}
	return event, nil
}
func GetEventByIdTransaction(tx *gorm.DB, id uuid.UUID) (models.Event, error) {
	var wallet models.Event
	var model models.Event
	err := tx.Model(&model).Where("id = ? && status = ?", id, repository_models.Active).Preload("WalletType").Preload("WalletType.Brand").First(&wallet).Error

	if errors.Is(err, gorm.ErrRecordNotFound) {
		return wallet, err
	}
	return wallet, nil
}

// UpdateEventById query for updating a event by given ID.
func UpdateEventById(u *models.Event, id uuid.UUID) error {
	// Send query to database.
	err := database.Database.Where("id = ? ", id).Updates(&u).Error
	return err
}

func UpdateDonationEventPointById(id uuid.UUID, point int) error {

	var model models.Event
	var event models.Event

	if err := database.Database.Where("id = ?", id).First(&event).Error; errors.Is(err, gorm.ErrRecordNotFound) {
		return err
	}

	err := database.Database.Model(&model).Where("id = ?", id).Updates(models.Event{NumberOfDonations: (event.NumberOfDonations + 1), TotalDonations: (event.TotalDonations + point)}).Error
	if errors.Is(err, gorm.ErrMissingWhereClause) {
		return err
	}
	return nil
}

func UpdateNumberOfCodesById(id uuid.UUID, number int) error {
	var model models.Event
	var event models.Event

	if err := database.Database.Where("id = ?", id).First(&event).Error; errors.Is(err, gorm.ErrRecordNotFound) {
		return err
	}

	err := database.Database.Model(&model).Where("id = ?", id).Updates(models.Event{NumberOfCodes: (event.NumberOfCodes + number)}).Error
	if errors.Is(err, gorm.ErrMissingWhereClause) {
		return err
	}
	return nil
}

func UpdateNumberOfAccumById(tx *gorm.DB, id uuid.UUID, number int) error {
	var model models.Event
	var event models.Event

	if err := tx.Where("id = ?", id).First(&event).Error; errors.Is(err, gorm.ErrRecordNotFound) {
		return err
	}

	err := tx.Model(&model).Where("id = ?", id).Updates(models.Event{NumberOfAccum: (event.NumberOfAccum + number)}).Error
	if errors.Is(err, gorm.ErrMissingWhereClause) {
		return err
	}
	return nil
}

func UpdateGeneratingCodeTaskID(id uuid.UUID, task_id string) error {
	var model models.Event
	var event models.Event

	if err := database.Database.Where("id = ?", id).First(&event).Error; errors.Is(err, gorm.ErrRecordNotFound) {
		return err
	}

	err := database.Database.Model(&model).Where("id = ?", id).Update("task_id", task_id).Error
	if errors.Is(err, gorm.ErrMissingWhereClause) {
		return err
	}
	return nil
}

func UpdateNumberOfSlotsById(id uuid.UUID, number int) error {
	var model models.Event
	var event models.Event

	if err := database.Database.Where("id = ?", id).First(&event).Error; errors.Is(err, gorm.ErrRecordNotFound) {
		return err
	}

	err := database.Database.Model(&model).Where("id = ?", id).Updates(map[string]interface{}{"number_of_slots": (event.NumberOfSlots + number)}).Error
	if errors.Is(err, gorm.ErrMissingWhereClause) {
		return err
	}
	return nil
}

func CreateEventCode(code *models.Code) error {
	err := database.Database.Create(&code).Error
	return err
}
