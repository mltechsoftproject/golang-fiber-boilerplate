package queries

import (
	"backend-qr-code/app/models"
	"backend-qr-code/platform/database"
	"backend-qr-code/services/auth"
	"backend-qr-code/services/common"
	"errors"
	"math"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

// UserQueries struct for queries from User model.

// GetUsers query for all users.
func GetUsers(filter *models.UserFilter, paging *common.Pagination) ([]models.User, error) {
	// Define User variable.
	user := []models.User{}
	db := database.Database
	if v := filter; v != nil {
		if v.Phone != "" {
			db = db.Where("phone =?", v.Phone)
		}
		if v.FullName != "" {
			db = db.Where("full_name LIKE ?", "%"+v.FullName+"%")
		}
		if v.Email != "" {
			db = db.Where("email like ?", "%"+v.Email+"%")
		}
		if v.UserRole != "" {
			db = db.Where("user_role = ?", v.UserRole)
		}
		if v.Gender != "" {
			db = db.Where("gender = ?", v.Gender)
		}
		if v.DonatePoint != 0 || v.EarnPoint != 0 || v.Point != 0 || v.WalletTypeID != "" {
			db = db.Joins("JOIN wallets ON wallets.user_id = users.id")
			if v.Point != 0 {
				db = db.Where("point = ?", v.Point)
			}
			if v.EarnPoint != 0 {
				db = db.Where("earn_point = ?", v.EarnPoint)
			}
			if v.DonatePoint != 0 {
				db = db.Where("donate_point = ?", v.DonatePoint)
			}
			if v.WalletTypeID != "" {
				db = db.Where("wallet_type_id = ?", v.WalletTypeID)
			}
		}
		if v.BrandID != "" {
			if v.DonatePoint != 0 || v.EarnPoint != 0 || v.Point != 0 || v.WalletTypeID != "" {
				db = db.Joins("JOIN wallet_types ON wallet_types.id = wallets.wallet_type_id")
			} else {
				db = db.Joins("JOIN wallets ON wallets.user_id = users.id").Joins("JOIN wallet_types ON wallet_types.id = wallets.wallet_type_id")
			}
			db = db.Where("wallet_types.brand_id = ?", v.BrandID)
		}
	}
	if paging != nil {
		db.Model(&models.User{}).Count(&paging.Total)
		paging.PageCount = int(math.Ceil(float64(paging.Total) / float64(paging.PageSize)))
		db = db.
			Offset((paging.Page - 1) * paging.PageSize).
			Limit(paging.PageSize)
	}
	// Send query to database.
	err := db.Model(&models.User{}).Preload("Wallets.WalletType.Brand").Find(&user).Error
	// Return query result.
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return user, err
	}
	return user, nil
}

// GetUserByID query for getting one User by given ID.
func GetUserByID(id uuid.UUID) (models.User, error) {
	// Define User variable.
	user := models.User{}
	// Define query string.
	// query := `SELECT * FROM users WHERE id = ?`
	// Send query to database.
	err := database.Database.Where("id = ?", id).Preload("Wallets.WalletType.Brand").Preload("Wallets.WalletType.Ranks").First(&user).Error
	// Return query result.
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return user, err
	}
	return user, nil
}

// GetUserByPhone query for getting one User by given Email.
func GetUserByPhone(phone string) (models.User, error) {
	// Define User variable.
	user := models.User{}
	// Define query string.
	// query := `SELECT * FROM users WHERE phone = ?`
	// Send query to database.
	err := database.Database.Where("phone = ?", phone).Preload("Wallets.WalletType.Brand").First(&user).Error
	// Return query result.
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return user, err
	}
	return user, nil
}

// CreateUser query for creating a new user by given email and password hash.
func CreateUser(u *models.User) error {
	// Send query to database.
	err := database.Database.Create(&u).Error

	return err
}

// UpdateUser query for updating a user by given ID.
func UpdateUser(u *models.UserUpdateProfile, user_id uuid.UUID) error {
	// Send query to database.
	err := database.Database.Where("id = ? ", user_id).Updates(&u).Error

	return err
}

func UpdatePassword(u *models.UserUpdatePassword, user_id uuid.UUID) error {
	// Send query to database.
	user := models.User{}
	err := database.Database.Model(&user).Where("id = ? ", user_id).Update("password_hash", auth.GeneratePassword(u.Password)).Error

	return err
}
