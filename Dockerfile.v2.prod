# Start from golang base image
FROM golang:1.18-alpine as builder

# ENV GO111MODULE=on

# Add Maintainer info
LABEL maintainer="Steven Victor <chikodi543@gmail.com>"

# Install git.
# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git

# Set the current working directory inside the container
WORKDIR /usr/src/app

# Copy go mod and sum files
COPY go.mod go.sum ./

# Download all dependencies. Dependencies will be cached if the go.mod and the go.sum files are not changed
RUN go mod download

# Copy the source from the current directory to the working Directory inside the container
COPY . .

# Build the Go app with VCS stamping disabled
RUN CGO_ENABLED=0 GOOS=linux go build -ldflags "-X main.version='' -s -w -buildid=''" -a -installsuffix cgo -o main .

# Start a new stage from scratch
FROM golang:1.18-alpine

# Install CA certificates
RUN apk --no-cache add ca-certificates

# Set the current working directory and change ownership to the new user
WORKDIR /usr/src/app_run

RUN mkdir /usr/src/app_run/file

# Copy the pre-built binary file from the previous stage and change ownership to the new user

COPY --from=builder /usr/src/app/main .
COPY --from=builder /usr/src/app/.env.production .
COPY --from=builder //usr/src/app/qr-code-75f24-firebase-sdk.json .

RUN chown -R 1000:1000 /usr/src/app_run

# Expose port 8888 to the outside world
EXPOSE 8888

# Switch to the new user and run the executable
USER 1000
CMD ["./main"]
