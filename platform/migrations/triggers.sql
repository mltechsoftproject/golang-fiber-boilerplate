CREATE DEFINER=`admin`@`%` TRIGGER onCreate
AFTER INSERT
ON codes FOR EACH ROW
BEGIN
	update events set number_of_codes = number_of_codes + 1 where id = new.event_id;
	END;

CREATE DEFINER=`admin`@`%` TRIGGER updateAccum
AFTER INSERT
ON transactions FOR EACH ROW
BEGIN
	IF (NEW.transaction_type = 'earn') THEN
		UPDATE events
		SET
			number_of_accum = number_of_accum + 1
		WHERE id = NEW.event_id;
	END IF;
	IF (NEW.transaction_type = 'donate') THEN
		UPDATE events
		SET
			number_of_donations = number_of_donations + 1,
			total_donations = total_donations + NEW.point
		WHERE id = NEW.event_id;
	END IF;
	IF (NEW.transaction_type = 'attendance') THEN
		UPDATE events
		SET
			number_of_accum = number_of_accum + 1
		WHERE id = NEW.event_id;
	END IF;
END
