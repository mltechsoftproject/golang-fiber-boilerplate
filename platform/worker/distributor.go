package worker

import (
	"backend-qr-code/app/models"
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/hibiken/asynq"
)

var WorkerTaskDistributor TaskDistributor

type TaskDistributor interface {
	DistributeCreateNewCodes(ctx *fiber.Ctx, payload *models.GenerateCodeWithEvent, opts ...asynq.Option) error
}

type RedisTaskDistributor struct {
	client *asynq.Client
}

func NewRedisTaskDistributor(redisOpt asynq.RedisClientOpt) TaskDistributor {
	client := asynq.NewClient(redisOpt)
	return &RedisTaskDistributor{
		client: client,
	}
}

func StartWorker() {
	const redisAddr = "qrcode-redis:6379"
	redisOpt := asynq.RedisClientOpt{Addr: redisAddr}

	taskDistributor := NewRedisTaskDistributor(redisOpt)
	WorkerTaskDistributor = taskDistributor

	taskProcessor := NewRedisTaskProcessor(redisOpt)
	log.Println("Start task processor")

	err := taskProcessor.Start()

	if err != nil {
		log.Println("Failed to start task processor")
	}
}
