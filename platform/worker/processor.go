package worker

import (
	"context"

	"github.com/hibiken/asynq"
)

type TaskProcessor interface {
	ProcessTaskGenerateCode(ctx context.Context, task *asynq.Task) error
	Start() error
}

type RedisTaskProcessor struct {
	server *asynq.Server
}

func NewRedisTaskProcessor(redisOpt asynq.RedisClientOpt) TaskProcessor {
	server := asynq.NewServer(redisOpt, asynq.Config{})
	return &RedisTaskProcessor{
		server: server,
	}
}

func (processor *RedisTaskProcessor) Start() error {
	mux := asynq.NewServeMux()
	mux.HandleFunc(TaskCreateEventCode, processor.ProcessTaskGenerateCode)
	return processor.server.Start(mux)
}
