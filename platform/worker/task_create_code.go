package worker

import (
	"backend-qr-code/app/models"
	"backend-qr-code/app/queries"
	repository_models "backend-qr-code/pkg/repository/models"
	"backend-qr-code/pkg/utils"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/hibiken/asynq"
)

const TaskCreateEventCode = "task:create_event_code"

func (distributor *RedisTaskDistributor) DistributeCreateNewCodes(ctx *fiber.Ctx, payload *models.GenerateCodeWithEvent, opts ...asynq.Option) error {

	jsonPayLoad, err := json.Marshal(payload)

	if err != nil {
		return fmt.Errorf("Failed to marshall: %w", err)
	}

	task := asynq.NewTask(TaskCreateEventCode, jsonPayLoad, opts...)

	info, err := distributor.client.EnqueueContext(ctx.Context(), task)

	if err != nil {
		return fmt.Errorf("Failed to enqueue task: %w", err)
	}

	queries.UpdateGeneratingCodeTaskID(payload.EventID, info.ID)

	log.Println("type: ", task.Type(), "payload: ", task.Payload(), "queue: ", info.Queue, "max_retry", info.MaxRetry)

	return nil
}

func (processor *RedisTaskProcessor) ProcessTaskGenerateCode(ctx context.Context, task *asynq.Task) error {
	requestBody := &models.GenerateCodeWithEvent{}

	startTime := time.Now().Nanosecond()

	if err := json.Unmarshal(task.Payload(), &requestBody); err != nil {
		return fmt.Errorf("failed to unmarshall: %w", asynq.SkipRetry)
	}
	var count = 0
	var countBefore = queries.CountNumberOfCodeByEventID(requestBody.EventID)
	for count < requestBody.Amount {
		var remain = requestBody.Amount - count
		// Loop by requestBody.Amount
		for i := 1; i <= remain; i++ {
			unique := utils.RandStringBytes(6)
			text := requestBody.Prefix + unique

			// Before validate - create model
			code := &models.Code{}

			// Set initialized default data:
			code.ID = uuid.New()
			code.CreatedAt = time.Now()
			code.UpdatedAt = time.Now()
			code.Status = repository_models.Active
			// Other data
			code.Code = text
			code.EventID = requestBody.EventID
			queries.CreateEventCode(code)
		}
		countAfter := queries.CountNumberOfCodeByEventID(requestBody.EventID)
		count = countAfter - countBefore
	}
	countAfterLoop := queries.CountNumberOfCodeByEventID(requestBody.EventID)

	if countAfterLoop != countBefore+requestBody.Amount {
		queries.UpdateNumberOfCodesById(requestBody.EventID, countBefore+requestBody.Amount)
	}
	endTime := time.Now().Nanosecond()
	log.Println("Time expense: " + strconv.Itoa(endTime-startTime))
	queries.UpdateGeneratingCodeTaskID(requestBody.EventID, "")
	return nil
}
