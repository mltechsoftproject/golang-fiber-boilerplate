package database

import (
	"backend-qr-code/app/models"
	"log"
	"os"

	"gorm.io/gorm"
)

var Database *gorm.DB

// OpenDBConnection func for opening database connection.
func OpenDBConnection() {
	// Define Database connection variables.
	var (
		db  *gorm.DB
		err error
	)

	// Get DB_TYPE value from .env file.
	dbType := os.Getenv("DB_TYPE")

	// Define a new Database connection with right DB type.
	switch dbType {
	case "mysql":
		db, err = MysqlConnection()
	}

	if err != nil {
		panic("Failed to connect to the database! " + err.Error())
	}

	log.Println("Connected to the database successfully!")

	// db.Logger = logger.Default.LogMode(logger.Info)

	log.Println("Running migrations")
	errDb := db.AutoMigrate(
		&models.User{},
		&models.Event{},
		&models.Code{},
	)

	if errDb != nil {
		panic(errDb)
	}
	Database = db
}
