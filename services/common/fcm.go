package common

import (
	"context"
	"log"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"google.golang.org/api/option"
)

var FCMClient *messaging.Client

func InitializeFirebaseApp() {
	ctx := context.Background()
	serviceAccountKey := "qr-code-75f24-firebase-sdk.json" // Replace with your own service account key file path

	config := &firebase.Config{
		ProjectID: "qr-code-75f24", // Replace with your Firebase project ID
	}

	// Initialize the app with the service account credentials
	app, err := firebase.NewApp(ctx, config, option.WithCredentialsFile(serviceAccountKey))
	if err != nil {
		log.Fatalf("Error initializing Firebase app: %v\n", err)
	}
	client, err := app.Messaging(ctx)
	if err != nil {
		log.Fatalf("Error initializing FCM client: %v\n", err)
	}
	FCMClient = client
	log.Println("Firebase app initialized successfully")
}
