package common

import "github.com/gofiber/fiber/v2"

type appResponse struct {
	Error      bool        `json:"error"`
	Msg        interface{} `json:"msg"`
	Data       interface{} `json:"data"`
	Pagination interface{} `json:"pagination,omitempty"`
	Filter     interface{} `json:"filter,omitempty"`
}

func NewSimpleResponse(c *fiber.Ctx, status int, msg, data interface{}) error {
	json := appResponse{Error: false, Msg: msg, Data: data, Pagination: nil, Filter: nil}
	return c.Status(status).JSON(json)
}

func NewSuccessResponse(c *fiber.Ctx, status int, msg, data, pagination, filter interface{}) error {
	json := appResponse{Error: false, Msg: msg, Data: data, Pagination: pagination, Filter: filter}
	return c.Status(status).JSON(json)
}

func NewErrResponse(c *fiber.Ctx, status int, msg interface{}) error {
	json := appResponse{Error: true, Msg: msg, Data: nil}
	return c.Status(status).JSON(json)
}
