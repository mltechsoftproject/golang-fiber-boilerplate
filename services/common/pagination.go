package common

type Pagination struct {
	Page      int   `json:"page" form:"page" query:"page"`
	PageSize  int   `json:"pageSize" form:"pageSize" query:"pageSize"`
	PageCount int   `json:"pageCount" form:"pageCount" query:"pageCount"`
	Total     int64 `json:"total" form:"total" query:"total"`
}

func (p *Pagination) Preload() {
	if p.Page <= 0 {
		p.Page = 1
	}

	if p.PageSize <= 0 {
		p.PageSize = 50
	}
}
