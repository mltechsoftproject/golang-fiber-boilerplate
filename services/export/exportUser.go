package services_export

import (
	"backend-qr-code/app/models"
	"bytes"
	"fmt"
	"log"
	"sync"

	"github.com/xuri/excelize/v2"
)

func writeUsersToFile(users []models.User, file *excelize.File) {
	sheetName := "Sheet1"
	headers := []string{"Tên người dùng", "Số điện thoại", "Email", "Giới tính", "Thời gian tạo", "Trạng thái"}

	//Create Default Excel Template
	CustomExcelTemplate(file, sheetName, headers, len(users))

	//Set Date style
	dateStyle, err := file.NewStyle(&excelize.Style{
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
		NumFmt: 22,
	})
	if err != nil {
		log.Println(err)
	}
	startDateCell := "A2"
	endDateCell := fmt.Sprintf("A%v", len(users)+1)

	if err := file.SetCellStyle(sheetName, startDateCell, endDateCell, dateStyle); err != nil {
		log.Println(err)
	}

	for i := 0; i < len(users); i++ {
		row := []interface{}{
			users[i].FullName,
			users[i].Phone,
			users[i].Email,
			users[i].Gender,
			users[i].CreatedAt.Format("2006-01-02 15:04:05"),
			users[i].Status,
		}
		for col, value := range row {
			cell := fmt.Sprintf("%c%d", 'A'+col, i+2)
			file.SetCellValue("Sheet1", cell, value)

			// Define border styles
			borderStyle, err := file.NewStyle(&excelize.Style{
				Border: []excelize.Border{
					{Type: "left", Color: "000000", Style: 1},
					{Type: "top", Color: "000000", Style: 1},
					{Type: "bottom", Color: "000000", Style: 1},
					{Type: "right", Color: "000000", Style: 1},
				},
				NumFmt: 22,
			})
			if err != nil {
				log.Println(err)
			}
			// Apply border style to the cell
			file.SetCellStyle("Sheet1", cell, cell, borderStyle)
		}
	}
}

func UserExportExcel(users []models.User, name string) (*bytes.Buffer, error) {
	f := excelize.NewFile()

	var wg sync.WaitGroup

	writeUsersToFileAsync := func() {
		defer wg.Done()
		writeUsersToFile(users, f)
	}

	wg.Add(1)
	go writeUsersToFileAsync()
	wg.Wait()

	buffer, err := f.WriteToBuffer()
	if err != nil {
		log.Println(err)
	}

	if err := f.Close(); err != nil {
		log.Println(err)
	}

	return buffer, nil
}
