package services_export

import (
	"log"

	"github.com/xuri/excelize/v2"
)

// CustomExcelTemplate ...
// The following code to create a new sheet with 'sheetName' and set style for header and body
func CustomExcelTemplate(file *excelize.File, sheetName string, headers []string, totalRows int) {
	//Set Column Width cho cột A -> Z
	if err := file.SetColWidth(sheetName, "A", "Z", float64(20)); err != nil {
		log.Println(err)
	}

	// Set Header
	//Set Header style
	headerStyle, err := file.NewStyle(&excelize.Style{
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 2},
			{Type: "top", Color: "000000", Style: 2},
			{Type: "bottom", Color: "000000", Style: 2},
			{Type: "right", Color: "000000", Style: 2},
		},
		Font:      &excelize.Font{Bold: true},
		Alignment: &excelize.Alignment{Horizontal: "center"},
		Fill:      excelize.Fill{Type: "pattern", Color: []string{"#F0F8FF"}, Pattern: 1},
	})
	if err != nil {
		log.Println(err)

	}
	//Convert headers to []interface{}
	headersName := make([]interface{}, len(headers))
	for i, v := range headers {
		headersName[i] = v
	}

	lastHeaderCell, err := excelize.CoordinatesToCellName(len(headers), 1)
	if err != nil {
		log.Println(err)
	}

	firstHeaderCell, err := excelize.CoordinatesToCellName(1, 1)
	if err != nil {
		log.Println(err)
	}

	if err := file.SetCellStyle(sheetName, firstHeaderCell, lastHeaderCell, headerStyle); err != nil {
		log.Println(err)
	}

	//Set Header Data
	if err := file.SetSheetRow(sheetName, firstHeaderCell, &headersName); err != nil {
		log.Println(err)
	}

	//Set Body
	//Set Body style
	bodyStyle, err := file.NewStyle(&excelize.Style{
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 2},
			{Type: "top", Color: "000000", Style: 2},
			{Type: "bottom", Color: "000000", Style: 2},
			{Type: "right", Color: "000000", Style: 2},
		},
	})
	if err != nil {
		log.Println(err)
	}

	firstBodyCell := "A2"
	lastBodyCell, err := excelize.CoordinatesToCellName(len(headersName), totalRows+1)
	if err != nil {
		log.Println(err)
	}

	if err := file.SetCellStyle(sheetName, firstBodyCell, lastBodyCell, bodyStyle); err != nil {
		log.Println(err)
	}

}
