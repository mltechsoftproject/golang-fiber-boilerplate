package services_export

import (
	"backend-qr-code/app/models"
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strconv"
	"sync"
	"time"
	"unicode/utf8"

	"github.com/xuri/excelize/v2"
)

var ExcelCharSet = string("ABCDEFGHIJKLMNOPQRSTUVWXYZ")

func writeCodeToFile(codes []models.Code, file *excelize.File, charSetIndex int, sheetName string) {
	for i := 0; i < len(codes); i++ {
		// Kiểm tra điều kiện cho từng phần tử trong mảng con
		code := codes[i].Code
		cell := string(ExcelCharSet[charSetIndex]) + strconv.Itoa(i+1)
		file.SetCellValue(sheetName, cell, code)
	}
}

func CodesExportExcel(codes []models.Code, name string) (string, error) {
	// Tạo file excel để ghi kết quả
	f := excelize.NewFile()

	// Kích thước của slice con
	batchSize := 100000

	var sliceCodes [][]models.Code

	for i := 0; i < len(codes); i += batchSize {
		end := i + batchSize
		if end > len(codes) {
			end = len(codes)
		}
		batch := codes[i:end]
		sliceCodes = append(sliceCodes, batch)
	}

	var wg sync.WaitGroup

	writeCodeToFileAsync := func(batchIndex int, sheetName string) {
		defer wg.Done()
		writeCodeToFile(sliceCodes[batchIndex], f, batchIndex, sheetName)
	}

	for i := 0; i < len(sliceCodes); i++ {
		wg.Add(1)
		go writeCodeToFileAsync(i, "Sheet1")
	}

	wg.Wait()

	tempFile := os.Getenv("STATIC_FILE_DIR") + name + "_" + time.Now().Local().Format("data-20060102150405.xlsx")
	if err := f.SaveAs(tempFile); err != nil {
		log.Println(err)
	}

	if err := f.Close(); err != nil {
		log.Println(err)
	}

	return tempFile, nil
}

func writeStreamCodeToFile(f *excelize.File, sheet *excelize.StreamWriter, codes []models.Code, charSetIndex int) error {
	styleId, err := f.NewStyle(&excelize.Style{
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 2},
			{Type: "top", Color: "000000", Style: 2},
			{Type: "bottom", Color: "000000", Style: 2},
			{Type: "right", Color: "000000", Style: 2},
		},
	})
	if err != nil {
		fmt.Println(err)
	}

	rowWidth := 0
	for i := 0; i < len(codes); i++ {
		if utf8.RuneCountInString(codes[i].Code) > rowWidth {
			rowWidth = utf8.RuneCountInString(codes[i].Code)
		}
	}
	rowWidth = rowWidth + 5
	if err := sheet.SetColWidth(1, rowWidth, float64(rowWidth)); err != nil {
		return err
	}

	for i := 0; i < len(codes); i++ {
		code := codes[i].Code
		cell := string(ExcelCharSet[0]) + strconv.Itoa(i+1)
		if err := sheet.SetRow(cell, []interface{}{excelize.Cell{StyleID: styleId, Value: code}}); err != nil {
			return err
		}
	}
	sheet.Flush()
	return nil
}

func CodesStreamExportExcel(codes []models.Code, name string) (string, error) {
	f := excelize.NewFile()

	batchSize := 500000
	var sliceCodes [][]models.Code

	for i := 0; i < len(codes); i += batchSize {
		end := i + batchSize
		if end > len(codes) {
			end = len(codes)
		}
		batch := codes[i:end]
		sliceCodes = append(sliceCodes, batch)
	}

	tempFile := os.Getenv("STATIC_FILE_DIR") + name + "_" + time.Now().Local().Format("data-20060102150405.xlsx")

	for i := 2; i <= len(sliceCodes); i++ {
		f.NewSheet(fmt.Sprintf("Sheet%d", i))
	}

	for i := 0; i < len(sliceCodes); i++ {
		sw, err := f.NewStreamWriter(fmt.Sprintf("Sheet%d", i+1))

		if err != nil {
			log.Println(err)
			return "", err
		}

		if err := writeStreamCodeToFile(f, sw, sliceCodes[i], i); err != nil {
			log.Println(err)
			return "", err
		}
	}

	if err := f.SaveAs(tempFile); err != nil {
		log.Println(err)
	}

	return tempFile, nil
}

func writeCodeToCSV(codes []models.Code, writer *csv.Writer) {
	for i := 0; i < len(codes); i += 10 {
		// Khởi tạo slice row mới cho mỗi hàng
		row := []string{}

		// Lặp qua 10 phần tử trong mảng codes hoặc ít hơn nếu không đủ phần tử để ghi
		for j := i; j < len(codes) && j < i+10; j++ {
			code := codes[j].Code
			row = append(row, code)
		}

		err := writer.Write(row)
		if err != nil {
			log.Println(err)
			return
		}
	}
}

func CodesExportCSV(codes []models.Code, name string) (string, error) {
	filePath := os.Getenv("STATIC_FILE_DIR") + name + "_" + time.Now().Local().Format("data-20060102150405.csv")
	file, err := os.Create(filePath)
	if err != nil {
		log.Println(err)
		return "", err
	}
	defer file.Close()

	writer := csv.NewWriter(file)

	header := []string{"Code"} // Thay bằng các trường dữ liệu tương ứng với codes
	err = writer.Write(header)
	if err != nil {
		log.Println(err)
		return "", err
	}

	batchSize := 500000
	var sliceCodes [][]models.Code

	for i := 0; i < len(codes); i += batchSize {
		end := i + batchSize
		if end > len(codes) {
			end = len(codes)
		}
		batch := codes[i:end]
		sliceCodes = append(sliceCodes, batch)
	}

	var wg sync.WaitGroup

	writeCodeToCSVAsync := func(batchIndex int) {
		defer wg.Done()
		writeCodeToCSV(sliceCodes[batchIndex], writer)
	}

	for i := 0; i < len(sliceCodes); i++ {
		wg.Add(1)
		go writeCodeToCSVAsync(i)
	}

	wg.Wait()

	writer.Flush()

	if err := writer.Error(); err != nil {
		log.Println(err)
		return "", err
	}

	return filePath, nil
}
