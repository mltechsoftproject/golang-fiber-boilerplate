package auth

import (
	repository_credentials "backend-qr-code/pkg/repository/credentials"
	repository_role "backend-qr-code/pkg/repository/role"
	"fmt"
)

// GetCredentialsByRole func for getting credentials from a role name.
func GetCredentialsByRole(role string) (map[string]bool, error) {
	// Define credentials variable.
	var credentials map[string]bool

	// Switch given role.
	switch role {
	case repository_role.AdminRoleName:
		// Admin credentials (all access).
		credentials = map[string]bool{
			repository_credentials.WalletCreateCredential:                false,
			repository_credentials.WalletUpdateCredential:                false,
			repository_credentials.UserGetListCredential:                 true,
			repository_credentials.WalletTypeCreateCredential:            true,
			repository_credentials.WalletTypeRankCreateCredential:        true,
			repository_credentials.WalletTypeUpdateCredential:            true,
			repository_credentials.WalletTypeDeleteCredential:            true,
			repository_credentials.WalletTypeUpdateStatusCredential:      true,
			repository_credentials.UserChangePasswordCredential:          true,
			repository_credentials.UserCreateBrandCredential:             true,
			repository_credentials.UserUpdateBrandCredential:             true,
			repository_credentials.EventCreateCredential:                 true,
			repository_credentials.EventUpdateCredential:                 true,
			repository_credentials.CodeCreateCredential:                  true,
			repository_credentials.CodeGetCredential:                     true,
			repository_credentials.UserCreateGroupNotificationCredential: true,
			repository_credentials.UserUpdateGroupNotificationCredential: true,
			repository_credentials.FeedbackGetCredential:                 true,
			repository_credentials.ExportExcel:                           true,
			repository_credentials.AdminGet:                              true,
		}

	case repository_role.StaffRoleName:
		// Moderator credentials (only creation and update).
		credentials = map[string]bool{
			repository_credentials.WalletCreateCredential:     false,
			repository_credentials.WalletUpdateCredential:     false,
			repository_credentials.WalletTypeCreateCredential: true,
			repository_credentials.WalletTypeUpdateCredential: true,
		}
	case repository_role.UserRoleName:
		// Simple user credentials (only creation).
		credentials = map[string]bool{
			repository_credentials.WalletCreateCredential:      true,
			repository_credentials.WalletUpdateCredential:      true,
			repository_credentials.TransactionCreateCredential: true,
			repository_credentials.WalletUpdatStatusCredential: true,
			repository_credentials.CodeGetCredential:           true,
			repository_credentials.FeedbackCreateCredential:    true,
		}
	default:
		// Return error message.
		return nil, fmt.Errorf("role '%v' does not exist", role)
	}

	return credentials, nil
}
