package auth

import (
	repository_role "backend-qr-code/pkg/repository/role"
	"fmt"
)

// VerifyRole func for verifying a given role.
func VerifyRole(role string) (string, error) {
	// Switch given role.
	switch role {
	case repository_role.AdminRoleName:
		// Nothing to do, verified successfully.
	case repository_role.StaffRoleName:
		// Nothing to do, verified successfully.
	case repository_role.UserRoleName:
		// Nothing to do, verified successfully.
	default:
		// Return error message.
		return "", fmt.Errorf("role '%v' does not exist", role)
	}

	return role, nil
}
