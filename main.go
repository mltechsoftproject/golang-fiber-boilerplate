package main

import (
	"os"

	"backend-qr-code/pkg/configs"
	"backend-qr-code/pkg/middleware"
	"backend-qr-code/pkg/routes"
	"backend-qr-code/pkg/utils"
	"backend-qr-code/platform/database"
	"backend-qr-code/platform/worker"
	"backend-qr-code/services/common"

	"github.com/gofiber/fiber/v2"
	// load .env file automatically
)

// @title API
// @version 1.0
// @description This is an auto-generated API Docs.
// @termsOfService http://swagger.io/terms/
// @contact.name API Support
// @contact.email your@mail.com
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @BasePath /api
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization

func main() {
	// Define Fiber config.
	config := configs.FiberConfig()

	// Define a new Fiber app with config.
	app := fiber.New(config)

	// Database innitialize
	database.OpenDBConnection()
	common.InitializeFirebaseApp()

	// Worker with Redis
	worker.StartWorker()

	// Middlewares.
	middleware.FiberMiddleware(app) // Register Fiber's middleware for app.

	// Routes.
	routes.MonitorRoute(app)  // Register route for Monitor middleware.
	routes.PublicRoutes(app)  // Register a public routes for app.
	routes.PrivateRoutes(app) // Register a private routes for app.
	routes.NotFoundRoute(app) // Register route for 404 Error.
	// Start server (with or without graceful shutdown).
	if os.Getenv("STAGE_STATUS") == "dev" {
		utils.StartServer(app)
	} else {
		utils.StartServerWithGracefulShutdown(app)
	}
}
